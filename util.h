#include <SDL2/SDL.h>
#include <stdio.h>
#include <cstdlib>   /* for atexit() */
#include <algorithm>
#include <iostream>
#include <vector>
#include <set>
#include <queue>
#include <iterator>
using std::swap;
using std::vector;
using namespace std;
#include <utility>
#include <cassert>


typedef vector<int> row;
typedef vector<row> board;
typedef vector<board> vectorOfBoards;


set<board> empty_cells(board b) {

	set<board> possibleBoards;
	//int numberOfBoardsadded = 0;


	for(size_t i = 0; i < b.size(); i++) {
    for(size_t j = 0; j < b[i].size(); j++){

			//270-411 is to check if the empty cells are at the corner

			if(b[0][0] == 0 && b[0][1] == 1 && b[1][0] != 0) {
				//in following lines I create a new board that eqivalent to the current one, and then I change the elements on this copy board in order to preserve the current board
         board chboard = b;
				chboard[0][0] = 1; 
				chboard[0][1] = 1;
				chboard[0][2] = 0;
				board b1 = chboard;
				    possibleBoards.insert(b1);
		}  if(b[0][0] == 0 && b[1][0] == 2 && b[1][0] != 0){
			   board chboard = b; //make a new board each time, should have original values nothign changed
			chboard[0][0] = 2;
			chboard[1][0] = 2;
			chboard[2][0] = 0;
			board b1 = chboard;
			possibleBoards.insert(b1);

		}

		if(b[0][0] == 0 && b[0][1] == 3 && b[1][0] != 0){
			board chboard = b;
			chboard[0][0] = 3;
			chboard[0][1] = 0;
			board b1 = chboard;
			possibleBoards.insert(b1);
		}

			if(b[0][0] == 0 && b[1][0] == 3 && b[0][1] != 0){
				board chboard = b;
			chboard[0][0] = 3;
			chboard[1][0] = 0;
			board b1 = chboard;
			possibleBoards.insert(b1);
		}

		if(b[0][3] == 0 && b[0][2] == 1){
			board chboard = b;
			chboard[0][3] = 1;
			chboard[0][2] = 1;
			chboard[0][1] = 0;
			board b1 = chboard;
			possibleBoards.insert(b1);
		}
		if(b[0][3] == 0 && b[1][3] == 2){
			board chboard = b;
			chboard[0][3] = 2;
			chboard[1][3] = 2;
			chboard[2][3] = 0;
			board b1 = chboard;
			possibleBoards.insert(b1);
		}

     if(b[0][3] == 0 && b[0][2] == 3){
			  board chboard = b;
       chboard[0][3] = 3;
			 chboard[0][2] = 0;
			 board b1 = chboard;
			 possibleBoards.insert(b1);
			 chboard = b;
		 }
		 if(b[0][3] == 0 && b[1][3] == 3){
			 board  chboard = b;
			 chboard[0][3] = 3;
			 chboard[1][3] = 0;
			 board b1 = chboard;
			 possibleBoards.insert(b1);
			 chboard = b;

		 }
       //hoizontal block at bottom left  corner
		 if(b[4][0] == 0 && b[4][1] == 1 && b[3][0] != 0){
			board chboard = b;
			 chboard[4][0] = 1;
			 chboard[4][1] = 1;
			 chboard[4][2] = 0;
			 board b1 = chboard;
			 possibleBoards.insert(b1);
			 chboard = b;
		 }
        //small square
		 if(b[4][0] == 0 && b[3][0] == 2 && b[4][1] != 0){
			 board chboard = b;
			 chboard[4][0] = 2;
			 chboard[3][0] = 2;
			 chboard[2][0] = 0;
			 board b1 = chboard;
			 possibleBoards.insert(b1);
			 chboard = b;
		 }

		 if(b[4][0] == 0 && b[4][1] == 3 && b[3][0] != 0 ){
			 board chboard = b;
			 chboard[4][0] = 3;
			 chboard[4][1] = 0;
			 board b1 = chboard;
			 possibleBoards.insert(b1);
			 chboard = b;
		 }
		 if(b[4][0] == 0 && b[3][0] == 3 &&  b[4][1] != 0 ){
			 board chboard = b;
			 chboard[4][0] = 3;
			 chboard[3][0] = 0;
			 board b1 = chboard;
			 possibleBoards.insert(b1);
			 chboard = b;
		 }

		  if(b[4][3] == 0 && b[4][2] == 1){
			 board chboard = b;
			 chboard[4][3] = 1;
			 chboard[4][2] = 1;
			 chboard[4][1] = 0;
			 board b1 = chboard;
			 possibleBoards.insert(b1);
			 chboard = b;
		 }

		if(b[4][3] == 0 && b[3][3] == 2){
			 board chboard = b;
			 chboard[4][3] = 2;
			 chboard[3][3] = 2;
			 chboard[2][3] = 0;
			 board b1 = chboard;
			 possibleBoards.insert(b1);
			 chboard = b;
		 }

		 if(b[4][3] == 0 && b[4][2] == 3){
			 board chboard = b;
			 chboard[4][3] = 3;
			 chboard[4][2] = 0;
			 board b1 = chboard;
			 possibleBoards.insert(b1);
			 chboard = b;
		 }
		 if(b[4][3] == 0 && b[3][3] == 3){
			 board chboard = b;
			 chboard[4][3] = 3;
			 chboard[3][3] = 0;
			 board b1 = chboard;
			 possibleBoards.insert(b1);
			 chboard = b;
		 }
      
		 //now move on to edge of the board

		 if( (0 <= j && j < 3) && (b[0][j] == 0) && ((b[0][j+1] != 0) &&   b[0][j-1] != 0)){

			        if(b[0][j+1] == 1) {
								board chboard = b;
								chboard[0][j] = 1;
								chboard[0][j+1] = 1;
								chboard[0][j+2] = 0;
								board b1 = chboard;
								possibleBoards.insert(b1);
								chboard = b;
							}

							if(b[0][j-1] == 1){
								board chboard = b;
								chboard[0][j] = 1;
								chboard[0][j-1] = 1;
								chboard[0][j-2] = 0;
								board b1 = chboard;
								possibleBoards.insert(b1);
							}



							if(b[1][j] == 2) {
								board chboard = b;
								chboard[0][j] = 2;
								chboard[1][j] = 2;
								chboard[2][j] = 0;
								board b1 = chboard;
								possibleBoards.insert(b1);

							}

							if(b[0][j+1] == 3){
								board chboard = b;
								chboard[0][j] = 3;
								chboard[0][j+1] = 0;
								board b1 = chboard;
								possibleBoards.insert(b1);
							}

							if(b[0][j-1] == 3){
								board chboard = b;
								chboard[0][j] = 3;
								chboard[0][j-1] = 0;
								board b1 = chboard;
								possibleBoards.insert(b1);

							}

						if(b[1][j] == 3){
							board chboard = b;
								chboard[0][j] = 3;
								chboard[1][j] = 0;
								board b1 = chboard;
								possibleBoards.insert(b1);
						}



		 }
  
			//this is looking at two spaces in the top edge of board
		 if((0 <= j && j < 3) && (i == 0)  && (b[i][j] == 0) && (b[i][j+1] == 0)){

			 if(b[i][j+2] == 1){
				 board chboard = b;
				 chboard[i][j+1] = 1;
				 chboard[i][j+2] = 1;
				 chboard[i][j+3] = 0;
				 board b1 = chboard;
						possibleBoards.insert(b1);
			 }

			 if(b[i][j+2] == 1){
				 board chboard = b;
				  chboard[i][j] = 1;
				 chboard[i][j+1] = 1;
				 chboard[i][j+2] = 0;
				 chboard[i][j+3] = 0;
				 board b1 = chboard;
				 possibleBoards.insert(b1);
			 }

			 if(b[i][j+2] == 3){
				  board chboard = b;
				  chboard[i][j+1] = 3;
				 chboard[i][j+2] = 0;
				 board b1 = chboard;
				 possibleBoards.insert(b1);
			 }

			  if(b[i][j+2] == 3){
				  board chboard = b;
				  chboard[i][j] = 3;
				 chboard[i][j+2] = 0;
				 board b1 = chboard;
				 possibleBoards.insert(b1);
			 }

			 //now blocks that can move in single space
			 if(b[i+1][j] == 2){
				    board chboard = b;
				  chboard[i][j] = 2;
				 chboard[i+1][j] = 2;
				 chboard[i+2][j] = 0;
				 board b1 = chboard;
				 possibleBoards.insert(b1);

			 }

			 if(b[i+1][j] == 3){
				  board chboard = b;
				  chboard[i][j] = 3;
				 chboard[i+1][j] = 0;
				 board b1 = chboard;
				 possibleBoards.insert(b1);

			 }

			 if(b[i+1][j+1] == 2){
					 board chboard = b;
				  chboard[i][j+1] = 2;
				 chboard[i+1][j+1] = 2;
				 chboard[i+2][j+1] = 0;
				 board b1 = chboard;
				 possibleBoards.insert(b1);

			 }

			 if(b[i+1][j+1] == 3){
				  board chboard = b;
				  chboard[i][j+1] =3;
				  chboard[i+1][j+1] =0;
				 board b1 = chboard;
				 possibleBoards.insert(b1);
			 }


			    if(b[i][j-1] == 3){
						board chboard = b;
						chboard[i][j] = 3;
						chboard[i][j-1] = 0;
						board b1 = chboard;
						possibleBoards.insert(b1);
					}

					if(b[i][j-1] == 1){
						board chboard = b;
						chboard[i][j] = 1;
						chboard[i][j-1] = 1;
						chboard [i][j-2] = 0;
						board b1 = chboard;
						possibleBoards.insert(b1);
					}

					if(b[i][j-1] == 1){
						board chboard = b;
						chboard[i][j+1] = 1;
						chboard[i][j] = 1;
						chboard[i][j-1] = 0;
						board b1 = chboard;
						possibleBoards.insert(b1);
					}

					//look at big square
					if(b[i+1][j] == 4){
						board chboard = b;
						chboard[i][j] = 4;
						chboard[i][j+1] = 4;
						chboard[i+1][j] = 4;
						chboard[i][j+1] = 4;
						chboard[i+2][j] = 0;
						chboard[i+2][j+1] = 0;
						board b1 = chboard;
						possibleBoards.insert(b1);
					}

							//have to consider possibility of horizontal rectangle moving up
				}

				   //now will move to left edge of board //single space
				if((0 < i && i < 4) && (b[i][0] == 0) && (b[i+1][0] != 0) && (b[i-1][0] != 0)){
					//if top is  vertical rectangle
					if( (i != 1) && b[i-1][0] == 2){
						board chboard = b;
						chboard[i][0] = 2;
						chboard[i-1][0] = 2;
						chboard[i-2][0] = 0;
						board b1 = chboard;
						possibleBoards.insert(b1);
					}

					if((i != 3) && b[i+1][0] == 2){
						board chboard = b;
						chboard[i][0] = 2;
						chboard[i+1][0] = 2;
						chboard[i+2][0] = 0;
						board b1 = chboard;
						possibleBoards.insert(b1);
					}
					//small square above and below empty space
					if(b[i+1][0] == 3){
						board chboard = b;
						chboard[i][0] = 3;
						chboard[i+1][0] = 0;
						board b1 = chboard;
						possibleBoards.insert(b1);

				}
				  if(b[i-1][0] == 3){
						board chboard = b;
						chboard[i][0] = 3;
						chboard[i-1][0] = 0;
						board b1 = chboard;
						possibleBoards.insert(b1);
					}

					//now look at horizontal at the side
					if(b[i][1] == 1){
						board chboard = b;
						chboard[i][0] = 1;
						chboard[i][1] = 1;
						chboard[i][2] = 0;
						board b1 = chboard;
						possibleBoards.insert(b1);
					}

					if(b[i][1] == 3){
						board chboard = b;
						chboard[i][0] = 3;
						chboard[i][1] = 0;
						board b1 = chboard;
						possibleBoards.insert(b1);

					}

					//now
		}

		 //now two empty spaces together on left side
		if((0 <= i && i < 4) && (j == 0) && (b[i][j] == 0) && (b[i+1][j] == 0)) {

			if(b[i][j+1] == 1){
				board chboard = b;
				chboard[i][j] = 1;
				chboard[i][j+1] = 1;
				chboard[i][j+2] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}

			if(b[i+1][j+1] == 1){
				 board chboard = b;
				chboard[i+1][j] = 1;
				chboard[i+1][j+1] = 1;
				chboard[i+1][j+2] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}

			//one movement for vertical
			if((i != 3 && i != 2) && b[i+2][j] == 2){
				board chboard = b;
				chboard[i+1][j] = 2;
				chboard[i+2][j] = 2;
				chboard[i+3][j] = 0;
                 board b1= chboard;
                 possibleBoards.insert(b1);
			}
			
		
			//two movements for vertical block at edge
			if(  (i != 3 && i != 2) && b[i+2][j] == 2){
				board chboard = b;
				chboard[i][j] = 2;
				chboard[i+1][j] = 2;
				chboard[i+2][j] = 0;
				chboard[i+3][j] = 0;
                 board b1= chboard;
                 possibleBoards.insert(b1);
			}

			//wow ok vertical block above empty space move one space
			if( (i != 0 && i != 1) && b[i-1][j] == 2 ){
			board chboard = b;
				chboard[i][j] = 2;
		    	chboard[i-1][j] = 2;
				chboard[i-2][j] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}
			//vertical block moves two spaces
			if((i != 0 && i != 1) && b[i-1][j] == 2){
				board chboard = b;
				chboard[i+1][j] = 2;
				chboard[i][j] = 2;
				chboard[i-1][j] = 0;
				chboard[i-2][j] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}


			   //vertical block moving sideways
			if(b[i][j+1] == 2){
				board chboard = b;
				chboard[i][j] = 2;
				chboard[i+1][j] = 2;
				chboard[i][j+1] = 0;
				chboard[i+1][j+1] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}

			//small square moving sideways with two open
			if(b[i][j+1] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i][j+1] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}
        //small block from side of second box opening
			if(b[i+1][j+1] == 3){
				board chboard = b;
				chboard[i+1][j] = 3;
				chboard[i+1][j+1] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}

			//small block moving up one space
			if((i != 3) && b[i+2][j] == 3){
				board chboard = b;
				chboard[i+1][j] = 3;
				chboard[i+2][j] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}
			 //moving up two spaces
			if( (i != 3) && b[i+2][j] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i+2][j] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}
			//small square moves down 1 and two spaces small block is above one space
			if( (i != 0) && b[i-1][j] == 3){
				 board chboard = b;
				chboard[i][j] = 3;
				chboard[i-1][j] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}

			//small block movex tw spaces with opening
			if((i != 0) && b[i-1][j] == 3){
				 board chboard = b;
				chboard[i+1][j] = 3;
				chboard[i-1][j] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}


			if(b[i][j+1] == 4){
				board chboard = b;
				chboard[i][j] = 4;
				chboard[i+1][j] = 4;
				chboard[i][j+1] = 4;
				chboard[i+1][j+1] = 4;
				chboard[i][j+2] = 0;
				chboard[i+1][j+2] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}

		}

		//now moving to right side

		//this next segment considers possibility where one box in the right edge is open
		if((0 < i && i < 4) && (j == 3) && (b[i][j] == 0) && (b[i-1][j] != 0) && (b[i+1][j] != 0)) {

			//left side is horizontal
			if(b[i][j-1] == 1){
				board chboard = b;
				chboard[i][j] = 1;
				chboard[i][j-1] = 1;
				chboard[i][j-2] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}
        //when vertical block is below
			if((i != 3) && b[i+1][j] == 2){
				board chboard = b;
				chboard[i][j] = 2;
				chboard[i+1][j] = 2;
				chboard[i+2][j] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}

			//vertical block above
			if((i != 0 && i != 1) && b[i-1][j] == 2){
				board chboard = b;
				chboard[i][j] = 2;
				chboard[i-1][j] = 2;
				chboard[i-2][j] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}
			//square to the left
			if(b[i][j-1] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i][j-1] = 0;
				chboard[i][j+2] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}
			//square above
			if(b[i+1][j] == 3){
				board chboard = b;
				chboard[i][j] = 1;
				chboard[i][j+1] = 1;
				chboard[i][j+2] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}
    //square below
			if(b[i-1][j] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i-1][j] = 3;
          board b1= chboard;
        possibleBoards.insert(b1);

			}

		}
      //two spaces being open on right
		if(( 0 <= i && i < 4) && (j == 3) && (b[i][j] == 0) && (b[i+1][j] == 0)){


		   	if(b[i][j-1] == 1){
				board chboard = b;
				chboard[i][j] = 1;
				chboard[i][j-1] = 1;
				chboard[i][j-2] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}

			//horizontal block at lower open space
			if(b[i+1][j-1] == 1){
				board chboard = b;
				chboard[i+1][j] = 1;
				chboard[i+1][j-1] = 1;
				chboard[i+1][j-2] = 0;
          board b1 = chboard;
        possibleBoards.insert(b1);
			}

     //one movement
			if( (i != 3 && i != 2) && b[i+2][j] == 2){
				 board chboard = b;
				chboard[i+1][j] = 2;
				chboard[i+2][j] = 2;
				chboard[i+3][j] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}

			  //two movements with vertical block
		if(   (i != 3) && b[i+2][j] == 2){
				 board chboard = b;
				chboard[i][j] = 2;
				chboard[i+1][j] = 2;
				chboard[i+2][j] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}

			//now look above the first sqare
			if( (i != 0 && i != 1) && b[i-1][j] == 2){
				 board chboard = b;
				chboard[i][j] = 2;
				chboard[i-1][j] = 2;
				chboard[i-2][j] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
		}
		  //two movements with vertical block above first opening
		if( (i != 0 && i != 1) && b[i-1][j] == 2){
			 board chboard = b;
				chboard[i+1][j] = 2;
				chboard[i][j] = 2;
				chboard[i-1][j] = 0;
				chboard[i-2][j] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
		}

		//now look at vertical block from the sie
	  if(b[i][j-1] == 2){
			 board chboard = b;
				chboard[i][j] = 2;
				chboard[i+1][j] = 2;
				chboard[i][j-1] = 0;
			chboard[i+1][j-1] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
		}

	  	//square  above
		if((i != 0) && b[i-1][j] == 3){
			 board chboard = b;
				chboard[i][j] = 3;
				chboard[i-1][j] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
		}

		//square above moving two spaces below
	   	if(  (i != 0) && b[i-1][j] == 3){
			 board chboard = b;
				chboard[i+1][j] = 3;
				chboard[i-1][j] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
		}

  //square to the left of first open space
		if(b[i][j-1] == 3){
			 board chboard = b;
				chboard[i][j] = 3;
				chboard[i][j-1] = 3;
          board b1= chboard;
        possibleBoards.insert(b1);
		}
		if(b[i+1][j-1] == 3){
			 board chboard = b;
				chboard[i+1][j] = 3;
				chboard[i+1][j-1] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
		}
		 //considering square below second box with  opening
		if( (i != 3) && b[i+2][j] == 3){
			board chboard = b;
				chboard[i+1][j] = 3;
				chboard[i+2][j] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
		}
		 //consider square below second opening that moves up two spaces;
		if( (i != 3) && b[i+2][j] == 3){
			board chboard = b;
				chboard[i][j] = 3;
				chboard[i+2][j] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
		}

		//large square moving in from the left side
		if(b[i][j-1] == 4 && b[i+1][j-1] == 4){
			board chboard = b;
				chboard[i][j] = 4;
				chboard[i+1][j] = 4;
			  chboard[i][j-1] = 4;
			chboard[i+1][j-1] = 4;
			chboard[i][j-2] = 0;
			chboard[i+1][j-2] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
		}
		}

			//now one openming on bottom edge that is not at the corner. I check to see that there isnt an empty space next to an empty one, basiclally two empty spaces
     
		if((0 < j && j < 3) && (i == 4) && (b[i][j] == 0) && (b[i][j-1] != 0) && (b[i][j+1] != 0)){

			if(b[i][j+1] == 1){
				board chboard = b;
				chboard[i][j] = 1;
				chboard[i][j+1] = 1;
				chboard[i][j+2] = 0;
				 board b1 = chboard;
        possibleBoards.insert(b1);
			}
			//horizontal block at left side
			if(b[i][j-1] == 1){
				board chboard = b;
				chboard[i][j] = 1;
				chboard[i][j-1] = 1;
				chboard[i][j-2] = 0;
				board b1 = chboard;
        possibleBoards.insert(b1);
			}

			//vertical block above
			if(b[i-1][j] == 2){
				board chboard = b;
				chboard[i][j] = 2;
				chboard[i-1][j] = 2;
				chboard[i-2][j] = 0;
				board b1 = chboard;
        possibleBoards.insert(b1);
			}

			//if small square is to the right
			if(b[i][j+1] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i][j+1] = 0;
          board b1 = chboard;
        possibleBoards.insert(b1);
			}
			//
			if(b[i][j-1] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i][j-1] = 0;
				board b1 = chboard;
        possibleBoards.insert(b1);
			}
			 //if horizontal squre above
			if(b[i-1][j] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i-1][j] = 0;
          board b1 = chboard;
        possibleBoards.insert(b1);
			}

			//if vertical square is above two spaces


		}

		//no for two spaces open at the bottm
		//checking if current space has a space next to it thats open

    

    
		if((0 <= j && j < 3) && (i == 4) && (b[i][j] == 0) && (b[i][j+1] == 0)){

			//horizontal block at right side move onespace
      if(b[i][j+2] == 1){
				board chboard = b;
				chboard[i][j+1] = 1;
				chboard[i][j+2] = 1;
				chboard[i][j+3] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}
			 //horizontal block to the rightt hat moves two spaces
			if(b[i][j+2] == 1){
				board chboard = b; //creating new board because I dont want to change the board that I am passing in because children states have to represent just one movement in all possible ways
				chboard[i][j] = 1;
				chboard[i][j+1] = 1;
				chboard[i][j+2] = 0;
				chboard[i][j+3] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}

			//horizontal to the left moves one  space
			if(b[i][j-1] == 1){
				board chboard = b;
				chboard[i][j] = 1;
				chboard[i][j-1] = 1;
				chboard[i][j-2] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}

			//horizontal to the left moves two spaces
			if(b[i][j-1] == 1){
					board chboard = b;
				chboard[i][j+1] = 1;
				chboard[i][j] = 1;
				chboard[i][j-1] = 0;
				chboard[i][j-2] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}

		 //horizontal above moves down, giving its starting at the beginning point
			if(b[i-1][j] == 1 && b[i-1][j+1] == 1) {
				   	board chboard = b;
				chboard[i][j] = 1;
				chboard[i][j+1] = 1;
				chboard[i-1][j] = 0;
				chboard[i-1][j+1] = 0;
          board b1= chboard;
        possibleBoards.insert(b1);
			}

			//now with vertical block above first square

			if(b[i-1][j] == 2){
				 board chboard = b;
				chboard[i][j] = 2;
				chboard[i-1][j] = 2;
				chboard[i-2][j] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

			//now vertical block above second square

			if(b[i-1][j+1] == 2){
				board chboard = b;
				chboard[i][j+1] = 2;
				chboard[i-1][j+1] = 2;
				chboard[i-2][j+1] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

		// now small square movementsone move
			if(b[i][j+2] == 3){
				board chboard = b;
				chboard[i][j+1] = 3;
				chboard[i][j+2] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}
			//two moves
			if(b[i][j+2] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i][j+2] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

			//square to the left of open space one move
			if(b[i][j-1] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i][j-1] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

			//square to the left movign all two spaces
			if(b[i][j-1] == 3){
				board chboard = b;
				chboard[i][j+1] = 3;
				chboard[i][j-1] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}
			 //horizontal block above first open space
			if(b[i-1][j] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i-1][j] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}
			//horizontal block above second open square
			if(b[i-1][j+1] == 3){
				board chboard = b;
				chboard[i][j+1] = 3;
				chboard[i-1][j+1] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

			//now large square //yay
			//no overlapping with blocks
			if(b[i-1][j] == 4 && b[i-1][j+1] == 4) {
				board chboard = b;
				chboard[i][j] = 4;
				chboard[i][j+1] = 4;
				chboard[i-1][j] = 4;
				chboard[i-1][j+1] = 4;
				chboard[i-2][j] = 0;
				 chboard[i-2][j+1] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}


		}
    
		//now cases where single space is open  on a non edge side ok..
		if((0 < i && i < 4) && (0 < j && j < 3) && (b[i][j] == 0) && (b[i][j+1] != 0) && (b[i][j-1] != 0) && (b[i-1][j] != 0) && (b[i+1][j] != 0)){

			if(b[i][j+1] == 1){
				board chboard = b;
				chboard[i][j] = 1;
				chboard[i][j+1] = 1;
				chboard[i][j+2] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}
			//horizontal block to the left
			if(b[i][j-1] == 1){
				board chboard = b;
				chboard[i][j] = 1;
				chboard[i][j-1] = 1;
				chboard[i][j-2] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);

			}

			//now vertical block above
			if( (i != 0 && i != 1) && b[i-1][j] == 2) {
				board chboard = b;
				chboard[i][j] = 2;
				chboard[i-1][j] = 2;
				chboard[i-2][j] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}
			  //now vertical block below
			if( (i != 3) && b[i+1][j] == 2){
				board chboard = b;
				chboard[i][j] = 2;
				chboard[i+1][j] = 2;
				chboard[i+2][j] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

				//small square all four directions
			if(b[i][j+1] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i][j+1] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

			if(b[i][j-1] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i][j-1] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

			if(b[i-1][j] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i-1][j] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

			if(b[i+1][j] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i+1][j] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

		}

		//now for horizontaldouble spaces not on the first two rows
		if((0 < i && i < 4) && (0 < j && j < 3) && (b[i][j] == 0) && (b[i][j+1] == 0)) {

			//horizontal coming from the right one move
			if(b[i][j+2] == 1){
				board chboard = b;
				chboard[i][j+1] = 1;
				chboard[i][j+2] = 1;
				chboard[i][j+3] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}
			 //horizontal on the right side of two spaces, two moves
			if(b[i][j+2] == 1){
				board chboard = b;
				chboard[i][j] = 1;
				chboard[i][j+1] = 1;
				chboard[i][j+2] = 0;
				chboard[i][j+3] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}
			//horizontal on the left side one move
			if(b[i][j-1] == 1){
				board chboard = b;
				chboard[i][j] = 1;
				chboard[i][j-1] = 1;
				chboard[i][j-2] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

			//horizontal on the left two moves
			if(b[i][j-1] == 1){
					board chboard = b;
				chboard[i][j+1] = 1;
				chboard[i][j] = 1;
				chboard[i][j-1] = 0;
				chboard[i][j-2] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

			//horizontal above this two spaces
			
				if(b[i-1][j] == 1 && b[i-1][j+1] == 1) {
					board chboard = b;
				chboard[i][j] = 1;
				chboard[i][j+1] = 1;
				chboard[i-1][j] = 0;
				chboard[i-1][j+1] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}
		
			//horizontal block below the two  spaces
			if(b[i+1][j] == 1){
				board chboard = b;
				chboard[i][j] = 1;
				chboard[i][j+1] = 1;
				chboard[i+1][j] = 0;
				chboard[i+1][j+1] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}
			//now vertical block below, left space
			if((i != 3) && b[i+1][j] == 2) {
				board chboard = b;
				chboard[i][j] = 2;
				chboard[i+1][j] = 2;
				chboard[i+2][j] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}
			//vertical block below right space
			if((i != 3) && b[i+1][j+1] == 2)  {
				board chboard = b;
				chboard[i][j+1] = 2;
				chboard[i+1][j+1] = 2;
				chboard[i+2][j+1] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}
			//
        //now small square to the right
			if(b[i][j+2] == 3){
				board chboard = b;
				chboard[i][j+1] = 3;
				chboard[i][j+2] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}
   //smalls move two spaced from the right
			if(b[i][j+2] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i][j+2] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

			//small square from the left
			if(b[i][j-1] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i][j-1] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}
			//from left two spaces
			if(b[i][j-1] == 3){
				board chboard = b;
				chboard[i][j+1] = 3;
				chboard[i][j-1] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

			//above the first left open square
			if(b[i-1][j] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i-1][j] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}
			//above second square
			if(b[i-1][j+1] == 3) {
				board chboard = b;
				chboard[i][j+1] = 3;
				chboard[i-1][j+1] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}
			  //below  the left open space
			if(b[i+1][j] == 3){
				board chboard = b;
				chboard[i][j] = 3;
				chboard[i+1][j] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

			//below right open space,second open box
			if(b[i+1][j+1] == 3) {
				board chboard = b;
				chboard[i][j+1] = 3;
				chboard[i+1][j+1] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

			//large square coming from above
			//making sure it doesnt overlap
			if( (i != 0 && i != 1) && b[i-1][j] == 4 && b[i-1][j+1] == 4) {
				board chboard = b;
				chboard[i][j] = 4;
				chboard[i][j+1] = 4;
				chboard[i-1][j] = 4;
				chboard[i-1][j+1] = 4;
				chboard[i-2][j] = 0;
				chboard[i-2][j+1] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}
				//big rectangle from below
			if(b[i+1][j] == 4 && b[i+1][j+1] == 4) {
				board chboard = b;
				chboard[i][j] = 4;
				chboard[i][j+1] = 4;
				chboard[i+1][j] = 4;
				chboard[i+1][j+1] = 4;
				chboard[i+2][j] = 0;
				chboard[i+2][j+1] = 0;
				board b1 = chboard;
				possibleBoards.insert(b1);
			}

			}

			//now when two spaces on top of each other and not at the edge and

			if((0 < i && i < 4) && (0 < j && j < 3) && (b[i][j] == 0) && (b[i+1][j] == 0)) {

				//horizontal block coming from rightt actually works y
					if(b[i][j+1] == 1){
						board chboard = b;
							chboard[i][j] = 1;
							chboard[i][j+1] = 1;
							chboard[i][j+2] = 0;
					board b1 = chboard;
					possibleBoards.insert(b1);
					}
				 //now from left
				if(b[i][j-1] == 1){
					board chboard = b;
							chboard[i][j] = 1;
							chboard[i][j-1] = 1;
							chboard[i][j-2] = 0;
					board b1 = chboard;
					possibleBoards.insert(b1);
				}
					//now horizontal from bottom right
				if(b[i+1][j+1] == 1){
					board chboard = b;
							chboard[i+1][j] = 1;
							chboard[i+1][j+1] = 1;
							chboard[i+1][j+2] = 0;
					board b1 = chboard;
					possibleBoards.insert(b1);
				}
				//no bottom left
				if(b[i+1][j-1] == 1){
					board chboard = b;
							chboard[i+1][j] = 1;
							chboard[i+1][j-1] = 1;
							chboard[i+1][j-2] = 0;
					board b1 = chboard;
					possibleBoards.insert(b1);
				}
				//possibility that below two squares is vertical one move
				if( (i != 3 && i != 4 && i != 2) && b[i+2][j] == 2) {
					board chboard = b;
							chboard[i+1][j] = 2;
							chboard[i+2][j] = 2;
							chboard[i+3][j] = 0;
					board b1 = chboard;
					possibleBoards.insert(b1);
				}
				 //below and moves both  squares
				if( (i != 3 && i != 4 && i != 2) && b[i+2][j] == 2){
					board chboard = b;
							chboard[i][j] = 2;
							chboard[i+1][j] = 2;
							chboard[i+2][j] = 0;
							chboard[i+3][j] = 0;
					board b1 = chboard;
					possibleBoards.insert(b1);
				}

				//possibility that vertical is above one move
				if( (i != 0 && i != 1) && b[i-1][j] == 2){
					board chboard = b;
							chboard[i][j] = 2;
							chboard[i-1][j] = 2;
							chboard[i-2][j] = 0;
					board b1 = chboard;
					possibleBoards.insert(b1);
				}
				//that vertical is  above, two moves
				if( (i != 0 && i != 1) && b[i-1][j] == 2){
					board chboard = b;
							chboard[i+1][j] = 2;
							chboard[i][j] = 2;
							chboard[i-1][j] = 0;
							chboard[i-2][j] = 0;
					board b1 = chboard;
					possibleBoards.insert(b1);
				}

				//vertical is to the side rigth sideways
				if(b[i][j+1] == 2) {
					    board chboard = b;
							chboard[i][j] = 2;
							chboard[i+1][j] = 2;
							chboard[i][j+1] = 0;
							chboard[i+1][j+1] = 0;
					board b1 = chboard;
					possibleBoards.insert(b1);
				}

				//verticalt to the left of square
				if(b[i][j-1] == 2){
					board chboard = b;
							chboard[i][j] = 2;
							chboard[i+1][j] = 2;
							chboard[i][j-1] = 0;
							chboard[i+1][j-1] = 0;
					board b1 = chboard;
					possibleBoards.insert(b1);
				}
				  //small squar e from top right
				if(b[i][j+1] == 3){
					board chboard = b;
						chboard[i][j] = 3;
						chboard[i][j+1] = 0;
					board b1= chboard;
					possibleBoards.insert(b1);
				}
				 //from top left
				if(b[i][j-1] == 3){
					board chboard = b;
						chboard[i][j] = 3;
						chboard[i][j-1] = 0;
					board b1= chboard;
					possibleBoards.insert(b1);
				}
				  //from bottom square right
				if(b[i+1][j+1] == 3) {
					board chboard = b;
						chboard[i+1][j] = 3;
						chboard[i+1][j+1] = 0;
					board b1= chboard;
					possibleBoards.insert(b1);
				}
						//bottom square left
				if(b[i+1][j-1] == 3) {
					board chboard = b;
						chboard[i+1][j] = 3;
						chboard[i+1][j-1] = 0;
					board b1= chboard;
					possibleBoards.insert(b1);
				}

				//now , possibiltiy of square bing under two open spaces

				if((i != 3) && b[i+2][j] == 3){
					board chboard = b;
						chboard[i+1][j] = 3;
						chboard[i+2][j] = 0;
					board b1= chboard;
					possibleBoards.insert(b1);
				}
				//two moves
				if(( i != 3) && b[i+2][j] == 3 ){
					board chboard = b;
						chboard[i][j] = 3;
						chboard[i+2][j] = 0;
					board b1= chboard;
					possibleBoards.insert(b1);
				}
				//being above first empty space and one move
				if((i != 0) && b[i-1][j] == 3){
					board chboard = b;
						chboard[i][j] = 3;
						chboard[i-1][j] = 0;
					board b1= chboard;
					possibleBoards.insert(b1);
				}
				//two moves
				if((i != 0) && b[i-1][j] == 3) {
					board chboard = b;
						chboard[i+1][j] = 3;
						chboard[i-1][j] = 0;
					board b1= chboard;
					possibleBoards.insert(b1);
				}

				//finally moves with squares larg  square
				if(b[i][j+1] == 4 && b[i+1][j+1] == 4){
					board chboard = b;
						chboard[i][j] = 4;
						chboard[i+1][j] = 4;
						chboard[i][j+1] = 4;
					  chboard[i+1][j+1] = 4;
						chboard[i][j+2] = 0;
						chboard[i+1][j+2] = 0;
					board b1= chboard;
					possibleBoards.insert(b1);
				}
				// now coming from left
				if(b[i][j-1] == 4 && b[i+1][j-1] == 4){
					board chboard = b;
						chboard[i][j] = 4;
						chboard[i+1][j] = 4;
						chboard[i][j-1] = 4;
					  chboard[i+1][j-1] = 4;
						chboard[i][j-2] = 0;
						chboard[i+1][j-2] = 0;
					board b1= chboard;
					possibleBoards.insert(b1);

				}
				//yay done with all possibilities

			}

		}


			}




return possibleBoards;

}