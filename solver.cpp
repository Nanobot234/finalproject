                                               /* compute optimal solutions for sliding block puzzle. */
#include <SDL2/SDL.h>
#include <stdio.h>
#include <cstdlib>   /* for atexit() */
#include <algorithm>
#include <iostream>
#include <vector>
#include <set>
#include <queue>
#include <iterator>
#include "util.h"  //this is my very long function to compute possible movements
using std::swap;
using std::vector;
using namespace std;
#include <utility>
#include <cassert>

/* SDL reference: https://wiki.libsdl.org/CategoryAPI */

/* initial size; will be set to screen size after window creation. */
int SCREEN_WIDTH = 640;
int SCREEN_HEIGHT = 480;
int fcount = 0;
int mousestate = 0;
SDL_Point lastm = {0,0}; /* last mouse coords */
SDL_Rect bframe; /* bnounding rectangle of board */
static const int ep = 2; /*epsilon offset from grid lines */

bool init(); /* setup SDL */
void initBlocks();
void drawConfiguration();
bool itsAttached = false;

typedef vector<int> row;
typedef vector<row> board;
typedef vector<board> vectorOfBoards;


//#define FULLSCREEN_FLAG SDL_WINDOW_FULLSCREEN_DESKTOP
 #define FULLSCREEN_FLAG 0




/* NOTE: ssq == "small square", lsq == "large square" */
enum bType {hor,ver,ssq,lsq};

struct block {
	SDL_Rect R; /* screen coords + dimensions */
	bType type;


	/* shape + orientation */
	/* TODO: you might want to add other useful information to
	 * this struct, like where it is attached on the board.
	 * (Alternatively, you could just compute this from R.x and R.y,
	 * but it might be convenient to store it directly.) */

		int& W = SCREEN_WIDTH;
	int& H = SCREEN_HEIGHT;
	int h = H*3/4;
	int w = 4*h/5;

	  bool isOnBoard() {

			 int maxX = bframe.x + (3 *(w/4));
			 int maxY = bframe.y + (4 * (h/5));

			if(((bframe.x <= R.x) && (R.x <= maxX)) && ((bframe.y <= R.y) && (R.y <= maxY))){
				return true;
			}else{
				return false;
			}

}

	void rotate() /* rotate rectangular pieces */
	{
		if (type != hor && type != ver) return;
		type = (type==hor)?ver:hor;
		swap(R.w,R.h);
	}
};


//the boardNode is a linkedlist element that has the current state as its data and points to a parent node, I try to use this to print the path to a 
//a solution but...
struct  boardNode {
public:
  boardNode* parent;
    board BoardData;
  
  //function to
  void setAsParent( boardNode *current, boardNode *parentnow){
      current->parent = parentnow;

}
};


      //function to create a boardnode

  boardNode* newBoardNode(board BoardData){
  boardNode* BoardNode = new boardNode();
  BoardNode->BoardData = BoardData;
  BoardNode->parent = NULL;
  return BoardNode;
}



#define RowBoardSquareBlocks 5
#define ColumnBoardSquareBlocks 4

#define NBLOCKS 10


block B[NBLOCKS];
block* dragged = NULL;

	///vector<block*> arrayOfDraggedBlocks;

block* findBlock(int x, int y);


void close(); /* call this at end of main loop to free SDL resources */
SDL_Window* gWindow = 0; /* main window */
SDL_Renderer* gRenderer = 0;

bool init()
{
	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL_Init failed.  Error: %s\n", SDL_GetError());
		return false;
	}
	/* NOTE: take this out if you have issues, say in a virtualized
	 * environment: */
	if(!SDL_SetHint(SDL_HINT_RENDER_VSYNC, "1")) {
		printf("Warning: vsync hint didn't work.\n");
	}
	/* create main window */
	gWindow = SDL_CreateWindow("Sliding block puzzle solver",
								SDL_WINDOWPOS_UNDEFINED,
								SDL_WINDOWPOS_UNDEFINED,
								SCREEN_WIDTH, SCREEN_HEIGHT,
								SDL_WINDOW_SHOWN|FULLSCREEN_FLAG);
	if(!gWindow) {
		printf("Failed to create main window. SDL Error: %s\n", SDL_GetError());
		return false;
	}
	/* set width and height */
	SDL_GetWindowSize(gWindow, &SCREEN_WIDTH, &SCREEN_HEIGHT);
	/* setup renderer with frame-sync'd drawing: */
	gRenderer = SDL_CreateRenderer(gWindow, -1,
			SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(!gRenderer) {
		printf("Failed to create renderer. SDL Error: %s\n", SDL_GetError());
		return false;
	}
	SDL_SetRenderDrawBlendMode(gRenderer,SDL_BLENDMODE_BLEND);

	initBlocks();
	//drawConfiguration();
	return true;
}

/* TODO: you'll probably want a function that takes a state / configuration
 * and arranges the blocks in accord.  This will be useful for stepping
 * through a solution.  Be careful to ensure your underlying representation
 * stays in sync with what's drawn on the screen... */

void makeboardArray(board& b){

	int& W = SCREEN_WIDTH; //640
	int& H = SCREEN_HEIGHT; //480
	int h = H*3/4; //360
	int w = 4*h/5;  //288
	int u = h/5-2*ep;
	//fill the

	bframe.x = (W-w)/2; // 176
	bframe.y = (H-h)/2; // 60 //392
	bframe.w = w;
	bframe.h = h;

	row theseROws(5); //
	for(size_t i = 0; i < 5; i++){

	b.push_back(theseROws);
	}

	int numofColumns = 4;
	for(size_t l = 0; l < b.size(); l++){
	    b[l].resize(numofColumns);
		}

/*
				board newb;

				newb.push_back({0, 4, 4, 2});
				newb.push_back({0, 4, 4, 2});
				newb.push_back({2, 3, 3, 3});	
				newb.push_back({2, 1, 1, 2});
				newb.push_back({3, 1, 1, 2});
						

	b = newb;
*/


		

    for(int i = 0; i < b.size(); i++) {
			for(int j = 0; j <b[i].size(); j++){
					int xCordinate = bframe.x + j*(w/4) ; // 3*(w/4)
				   int yCordinate = bframe.y + i*(h/5);  //h/5
				//68 of one box hhiehgt with is 70

				for(int r = 0; r < 10; r++) {

					
			

		
					int greatery = yCordinate + 10;
					int greaterx = xCordinate + 5;
						if((xCordinate <= B[r].R.x && B[r].R.x <= greaterx ) &&  (yCordinate <= B[r].R.y && B[r].R.y <= greatery) && (B[r].type == hor)) {
							//cout << B[i].R.x << " ";
							b[i][j] = 1;
							b[i][j+1] = 1;

						}
  //looks at the starting point of rectangle
						if((xCordinate <= B[r].R.x && B[r].R.x <= greaterx ) && ( yCordinate <= B[r].R.y &&  B[r].R.y <= greatery) && (B[r].type == ver)) {
							b[i][j] = 2;
							b[i+1][j] = 2;
						}
						if((xCordinate <= B[r].R.x && B[r].R.x <= greaterx ) && ( yCordinate <= B[r].R.y && B[r].R.y <= greatery) && (B[r].type == ssq)) {
							b[i][j] = 3;

						}
	if((xCordinate <= B[r].R.x && B[r].R.x <= greaterx ) &&  (yCordinate <= B[r].R.y && B[r].R.y <= greatery) && (B[r].type == lsq))  {
							b[i][j] = 4;
							b[i][j+1] = 4;
							b[i+1][j] = 4;
							b[i+1][j+1] = 4;
						}



				}

			if(b[i][j] == 0) {
							b[i][j] = 0;
						}

			}
		}
		


}

//basically after this I was going to compute possibilites of movement?


//will return a set?? containing all possible boards
//my idea search through by two d array,

//in this drawConfiguration method,  manually set the block coordinates to be the same as the upperright coordinates from where the block was drawn. This created a default starting configuration which I will edit.



void initBlocks()
{
	int& W = SCREEN_WIDTH;
	int& H = SCREEN_HEIGHT;
	int h = H*3/4;
	int w = 4*h/5;
	int u = h/5-2*ep;
	int mw = (W-w)/2; //MID-WIDTH
	int mh = (H-h)/2; //444444444444mid-height

	/* setup bounding rectangle of the board: */
	bframe.x = (W-w)/2;
	bframe.y = (H-h)/2;
	bframe.w = w;
	bframe.h = h;

	/* NOTE: there is a tacit assumption that should probably be
	 * made explicit: blocks 0--4 are the rectangles, 5-8 are small
	 * squares, and 9 is the big square.  This is assumed by the
	 * drawBlocks function below. */

	for (size_t i = 0; i < 5; i++) {

		B[i].R.x = (mw-2*u)/2;
		B[i].R.y = mh + (i+1)*(u/5) + i*u;
		B[i].R.w = 2*(u+ep);
		B[i].R.h = u;
		B[i].R.w = 2*(u+ep);
		B[i].R.h = u;
		B[i].type = hor;
	}

	for (size_t i = 0; i < 4; i++) {
		B[i+5].R.x = (W+w)/2 + (mw-2*u)/2 + (i%2)*(u+u/5);
		B[i+5].R.y = mh + ((i/2)+1)*(u/5) + (i/2)*u;
		B[i+5].R.w = u;
		B[i+5].R.h = u;
		B[i+5].type = ssq;
	}

	B[9].R.x = B[5].R.x + u/10;
	B[9].R.y = B[7].R.y + u + 2*u/5;
	B[9].R.w = 2*(u+ep);
	B[9].R.h = 2*(u+ep);
	B[9].type = lsq;


}
void drawBlocks()
{
	/* rectangles */
	SDL_SetRenderDrawColor(gRenderer, 0x43, 0x4c, 0x5e, 0xff);
	for (size_t i = 0; i < 5; i++) {
		SDL_RenderFillRect(gRenderer,&B[i].R);
	}
	/* small squares */
	SDL_SetRenderDrawColor(gRenderer, 0x5e, 0x81, 0xac, 0xff);
	for (size_t i = 5; i < 9; i++) {
		SDL_RenderFillRect(gRenderer,&B[i].R);
	}
	/* large square */
	SDL_SetRenderDrawColor(gRenderer, 0xa3, 0xbe, 0x8c, 0xff);
	SDL_RenderFillRect(gRenderer,&B[9].R);
}

/* return a block containing (x,y), or NULL if none exists. */

block* findBlock(int x, int y)
{
	/* NOTE: we go backwards to be compatible with z-order */
	for (int i = NBLOCKS-1; i >= 0; i--) {
		//basically if x coordinate is in between R.x and R.x + R.w
		if (B[i].R.x <= x && x <= B[i].R.x + B[i].R.w &&
				B[i].R.y <= y && y <= B[i].R.y + B[i].R.h)

			return (B+i);
	}
	return NULL;
}

void close()
{
	SDL_DestroyRenderer(gRenderer); gRenderer = NULL;
	SDL_DestroyWindow(gWindow); gWindow = NULL;
	SDL_Quit();
}

void render()
{
	/* draw entire screen to be black: */
	SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xff);
	SDL_RenderClear(gRenderer);

	/* first, draw the frame: */
	int& W = SCREEN_WIDTH;
	int& H = SCREEN_HEIGHT;
	int w = bframe.w;
	int h = bframe.h;
	SDL_SetRenderDrawColor(gRenderer, 0x39, 0x39, 0x39, 0xff);
	SDL_RenderDrawRect(gRenderer, &bframe);
	/* make a double frame */
	SDL_Rect rframe(bframe);
	int e = 3;
	rframe.x -= e;
	rframe.y -= e;
	rframe.w += 2*e;
	rframe.h += 2*e;
	SDL_RenderDrawRect(gRenderer, &rframe);

	/* draw some grid lines: */
	SDL_Point p1,p2;
	SDL_SetRenderDrawColor(gRenderer, 0x19, 0x19, 0x1a, 0xff);
	/* vertical */
	p1.x = (W-w)/2;
	p1.y = (H-h)/2;
	p2.x = p1.x;
	p2.y = p1.y + h;
	for (size_t i = 1; i < 4; i++) {
		p1.x += w/4;
		p2.x += w/4;
		SDL_RenderDrawLine(gRenderer,p1.x,p1.y,p2.x,p2.y);
	}

	/* horizontal */
	p1.x = ((W-w)/2);
	p1.y = ((H-h)/2);
	p2.x = p1.x + w;
	p2.y = p1.y;
	for (size_t i = 1; i < 5; i++) {
		p1.y += h/5;
		p2.y += h/5;
		SDL_RenderDrawLine(gRenderer,p1.x,p1.y,p2.x,p2.y);
	}
	SDL_SetRenderDrawColor(gRenderer, 0xd8, 0xde, 0xe9, 0x7f);
	SDL_Rect goal = {bframe.x + w/4 + ep, bframe.y + 3*h/5 + ep,
	                 w/2 - 2*ep, 2*h/5 - 2*ep};
	SDL_RenderDrawRect(gRenderer,&goal);

	/* now iterate through and draw the blocks */
	drawBlocks();
	/* finally render contents on screen, which should happen once every
	 * vsync for the display */
	SDL_RenderPresent(gRenderer);

								 }

		//function to print a given board to standard out
		void Print_Boards(board g) {

					for(size_t i = 0; i < g.size(); i++){
						for(size_t j = 0; j < g[i].size(); j++){

							cout << g[i][j] << " ";

						}
						}

						cout << endl;


				}

//function to check if current boaed is the soltuion
				bool isTheGoal(board b){

		for(size_t i = 0; i < b.size(); i++){
			for(size_t j = 0; j < b[i].size(); j++){

				if( b[3][1] == 4 && b[3][2] == 4 && b[4][1] == 4 && b[4][2] == 4){
					return true;
				}
				else {
					return false;
				}
			}


		}

	}

//this function checks to see if a board is in an array of examined board already. This is to prevent reexcaminin 
	bool IsAlreadyExamined(vectorOfBoards vec, board b){
		if(vec.empty()){
			return false;
		}
  if(std::find(vec.begin(),vec.end(),b) != vec.end()){
		return true;
	} else{
		return false;
	}


	}


void snap(block* b)
{
	/* TODO: once you have established a representation for configurations,
	 * you should update this function to make sure the configuration is
	 * updated when blocks are placed on the board, or taken off.  */
	assert(b != NULL);
	/* upper left of grid element (i,j) will be at
	 * bframe.{x,y} + (j*bframe.w/4,i*bframe.h/5) */
	/* translate the corner of the bounding box of the board to (0,0). */
	int x = b->R.x - bframe.x; // xdistance to frame
	int y = b->R.y - bframe.y; // ydistance to frame
	int uw = bframe.w/4;
	int uh = bframe.h/5;
	/* NOTE: in a perfect world, the above would be equal. */
	int i = (y+uh/2)/uh; /* row */ //ydistance + row distance/
	int j = (x+uw/2)/uw; /* col */
	if (0 <= i && i < 5 && 0 <= j && j < 4) {
		b->R.x = bframe.x + j*uw + ep;
		b->R.y = bframe.y + i*uh + ep;
	}

	 itsAttached == true;
}

int main(int argc, char *argv[])
{
	/* TODO: add option to specify starting state from cmd line? */
	/* start SDL; create window and such: */

	board GameBoard;

	//makeboardArray(GameBoard);
	int allIsOnBoard = 0; //this is counter for number of blocks on board

	if(!init()) {
		printf( "Failed to initialize from main().\n" );
		return 1;
	}
	atexit(close);
	bool quit = false; /* set this to exit main loop. */
	SDL_Event e;
	/* main loop: */
	while(!quit) {
		/* handle events */

		while(SDL_PollEvent(&e) != 0) {
			/* meta-q in i3, for example: */
			if(e.type == SDL_MOUSEMOTION) {
				if (mousestate == 1 && dragged) {
					int dx = e.button.x - lastm.x;
					int dy = e.button.y - lastm.y;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					dragged->R.x += dx;
					dragged->R.y += dy;
				}
			} else if (e.type == SDL_MOUSEBUTTONDOWN) {
				if (e.button.button == SDL_BUTTON_RIGHT) {
					block* b = findBlock(e.button.x,e.button.y);
					if (b) b->rotate();
				} else {
					mousestate = 1;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					dragged = findBlock(e.button.x,e.button.y);
				}
				/* XXX happens if during a drag, someone presses yet
				 * another mouse button??  Probably we should ignore it. */
			} else if (e.type == SDL_MOUSEBUTTONUP) {
				if (e.button.button == SDL_BUTTON_LEFT) {
					mousestate = 0;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					if (dragged) {
						int previousX = dragged->R.x;
						snap(dragged);

							allIsOnBoard++;
						if(allIsOnBoard >= NBLOCKS) {//== NBLOCKS){
							makeboardArray(GameBoard);
							continue;
						}

						//arrayOfDraggedBlocks.push_back(dragged);
					}
					dragged = NULL;
				}
			} else if (e.type == SDL_QUIT) {
				quit = true;
			} else if (e.type == SDL_KEYDOWN) {
				switch (e.key.keysym.sym) {
					case SDLK_ESCAPE:
					case SDLK_q:
						quit = true;
						break;
					case SDLK_LEFT:
						/* TODO: show previous step of solution */
						break;
					case SDLK_RIGHT:
						/* TODO: show next step of solution */
						break;
					case SDLK_p:
						/* TODO: print the state to stdout
						 * (maybe for debugging purposes...) */
						break;
					case SDLK_s:
						/* TODO: try to find a solution */
				//que
				
					{
					 set<board> printStates;
					 queue<board> queueOfBoards;
					queueOfBoards.push(GameBoard);

			vectorOfBoards vecBoard;

				int numberOfSteps = 0;
							

					//queue of Boardss
	 		while(!queueOfBoards.empty()){

	
		 board currentGameBoard = queueOfBoards.front();
  
    //a node with the currentGameBoard as the data
    			boardNode* motherBoard = newBoardNode(currentGameBoard);
    //all the children configurations
    				boardNode* Board;
				 queueOfBoards.pop();

				if(IsAlreadyExamined(vecBoard, currentGameBoard) != true){

           				 
						  vecBoard.push_back(currentGameBoard);
    

		 if(isTheGoal(currentGameBoard) == true){
			cout << "Found the solution after " << numberOfSteps << " steps";
      cout << endl;
      //this function was to print path from goal node to initial config in 
      		cout << "Final board state was";
      		 Print_Boards(currentGameBoard);


      while(motherBoard->parent != NULL){
          Print_Boards(motherBoard->BoardData);
           motherBoard = motherBoard->parent;      //////
           cout << endl;
      }
      break;

		}   else {
			numberOfSteps++;
			//printStates is the set of all possible moves from a given configuration
			printStates = empty_cells(currentGameBoard);
				//below I iterate through 
			set<board>:: iterator itr;
			for(itr = printStates.begin(); itr != printStates.end(); ++itr){ 
		 				 queueOfBoards.push(*itr);
       					Board = newBoardNode(*itr);
     			 Board->setAsParent(Board, motherBoard);
      
	}

		}

	}
}
						break;
					}
					

					default:
						break;
				}
			}
		
		fcount++;
		render();
	}






	//bType {hor,ver,ssq,lsq}

}

  			//Print_Boards(GameBoard);

	printf("total frames rendered: %i\n",fcount);


//	printf("Size is %d\n",size);
	return 0;

}

